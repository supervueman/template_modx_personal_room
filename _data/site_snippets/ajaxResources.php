id: 50
source: 1
name: ajaxResources
properties: 'a:0:{}'

-----

// Отвечаем ТОЛЬКО на ajax запросы
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {return;}

// данный код можно расширить добавив другие действия и указать их в case
$action = filter_input(INPUT_POST,'action');

// Если в массиве POST нет действия - выход
if (empty($action)) {return;}

// А если есть - работаем
$res = '';
switch ($action) {
  case 'getProduct':
    // Задаём параметры для сниппета в AJAX запросе
    $params = array();
    $params['parents'] = 6;
    $params['depth'] = 1;
    $params['element'] = 'pdoResources';
    $params['tpl'] = filter_input(INPUT_POST, 'tpl');
    $params['resources'] = filter_input(INPUT_POST, 'product');
    $params['showUnpublished'] = 1;
    $params['includeTVs'] = 'title, subtitle, product_images, product_short_desc';

    $res = $modx->runSnippet('msProducts', $params); break;
}
// Если у нас есть, что отдать на запрос - отдаем и прерываем работу парсера MODX
if (!empty($res)) {
  die($res);
}