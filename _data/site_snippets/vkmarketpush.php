id: 41
source: 1
name: vkmarketpush
properties: 'a:0:{}'

-----

// https://oauth.vk.com/authorize?client_id=171625293&display=page&redirect_uri=https://sivak-show.ru/&scope=????&response_type=code&v=5.85
// https://oauth.vk.com/access_token?client_id=6702247&client_secret=ST4dYKADIHWkDs1H2raG&redirect_uri=https://sivak-show.ru/&code=????
function market() {
  $group_id = 171739883;
  $user_id = 171625293;
  $market_access = 'e9d12504fba11671132030637fa0207bb5ad562c39bdc03a8fbd0ea19eeaf087691052fd20816a1b2cf5a';
  $frends_access = '85e5fd4c872889d4e5e8e9bc52ec65e812e06d4575b39bb84593ed7f7569f94025a123de43efe486deaf9';
  $photos_access = '4d95b9ce9efde41ce828f3368ae3f27b8ac8967182f7608ecaaf8be8f8205ba5c4c7c6e8e24f67c6121e5';
  $photos_market_access = 'abe4e0b9ca62448d4877f1278d8bce386ce58ea7d2bad9eeb4c4908f8226110dbf5d075d379c0ad002deb';
  $url = 'https://api.vk.com/method/';
  $method_mg = 'market.get?';
  $method_phGetServer = 'photos.getMarketUploadServer?';
  $v = '5.85';
  $file = 'assets/img/item.png';
  $cfile = curl_file_create($file,'image/png','item-5.png');

  // $request_params = array(
  // 'owner_id' => '-'.$group_id,
  // 'v' => $v,
  // 'access_token' => $market_access
  // );
  // $get_params = http_build_query($request_params);
  // $result = json_decode(file_get_contents('https://api.vk.com/method/market.get?'. $get_params));
  // var_dump($result -> response -> items);
  // $img = $result -> response -> items[0] -> thumb_photo;
  // echo $img;
  // echo "<img src='$img'></img>";
  
  $request_params = array(
    'group_id' => $group_id,
    'main_photo' => 1,
    'v' => $v,
    'access_token' => $photos_market_access
  );
  $get_params = http_build_query($request_params);
  $result = json_decode(file_get_contents('https://api.vk.com/method/photos.getMarketUploadServer?'.$get_params));
  $upload_url = $result -> response -> upload_url;
  // echo($upload_url);

  $ch = curl_init();
  // Поля POST-запроса
  $parameters = array(
    'file' => $cfile // PHP >= 5.5.0
  );
  curl_setopt_array($ch, array(
    CURLOPT_POST => true, // Говорим cURL, что это POST-запрос
    CURLOPT_RETURNTRANSFER => true, // Говорим cURL, что нам нужно знать, что ответит сервер, к которому мы будем обращаться
    CURLOPT_POSTFIELDS => $parameters, // Говорим cURL, какие поля будем отправлять
    CURLOPT_URL => $upload_url, // Ссылка, куда будем загружать картинку - это upload_url
    CURLOPT_HTTPHEADER => array('Content-Type: multipart/form-data')
  ));
  // Выполняем cURL-запрос. В этой переменной будет JSON-ответ от ВКонтакте
  $curl_result = curl_exec($ch);
  $err = curl_error($ch);
  // Закрываем соединение
  curl_close($ch);
  // echo($curl_result);

  $result = json_decode($curl_result);
  $server = $result -> server;
  $photo = stripslashes($result -> photo);
  $hash = $result -> hash;
  $crop_data = $result -> crop_data;
  $crop_hash = $result -> crop_hash;
  
  // var_dump($result);
  
  $save_params = array(
    'group_id' => $group_id,
    'photo' => $photo,
    'server' => $server,
    'hash' => $hash,
    'crop_data' => $crop_data,
    'crop_hash' => $crop_hash,
    'v' => $v,
    'access_token' => $photos_market_access
  );
  $get_save_params = http_build_query($save_params);
  $photo_data = json_decode(file_get_contents('https://api.vk.com/method/photos.saveMarketPhoto?'.$get_save_params));
  
  $name = 'MOOORBOOO';
  $main_photo_id = $photo_data -> response[0] -> id;
  $description = 'Next news f*cking human';
  $category_id = 103;
  $price = 400000;

  $prod_params = array(
    'owner_id' => '-'.$group_id,
    'name' => $name,
    'description' => $description,
    'category_id' => $category_id,
    'price' => $price,
    'main_photo_id' => $main_photo_id,
    'access_token' => $photos_market_access,
    'v' => $v,
  );
  $get_prod_params = http_build_query($prod_params);
  $prod_data = json_decode(file_get_contents('https://api.vk.com/method/market.add?'.$get_prod_params));
  print_r($prod_data);
}
market();

// Сниппет для вывода названий файлов из директории
// $dir = 'assets/img';
// if ($handle = opendir($dir)){
//   while ($file = readdir($handle)){
//     for ($i=0; $i<count($file); $i++) 
//       if ($file[$i] != '.' && $file[$i] != '..') {
//         echo $file . '<br />';
//     }
//   }
//   closedir($handle);
// }