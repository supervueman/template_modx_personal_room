$(document).ready(function() {
	// Mobile menu
	let mobile_menu = $('#mob-mnu');
	mobile_menu.mmenu({
		btn: '#hamburger',
		close_elements: '.blocker',
	});
	mobile_menu
		.find('menu')
		.find('.link')
		.click(function() {
			mobile_menu.removeClass('.active-menu');
			$('body').removeClass('mob-mnu-active');
			$('#hamburger').removeClass('hamburger-active');
		});

	// LinkActivator
	let linkActivator = new LinkActivator({
		scrollContainer: '#scroll-container',
		link: '.link',
		sect: '.sect',
	});
	linkActivator.init();

	// Animator
	let arrDataNum = [],
		bool = false;
	let animator = new Animator({
		scrollContainer: '#scroll-container',
		elems: '.anim-elem',
		animator: 'animator',
		offsetPercent: 80,
		handler: function() {},
	});
	animator.init();

	// Progress
	let toolbar = $('.toolbar');
	$('.screen-scroll').scroll(function() {
		let scrollTop = $(this)[0].scrollTop,
			scrollHeight = $(this)[0].scrollHeight,
			height = $(window).height(),
			progress = (100 * scrollTop) / (scrollHeight - height);
		$('.progress').css('width', `${progress}%`);
		if (scrollTop > 100) {
			toolbar.addClass('active-toolbar');
		} else {
			toolbar.removeClass('active-toolbar');
		}
	});

	// Modal
	// Modal
	$('.open-modal').modal({
		btn_close: '.close-modal',
		from: '#Hidden__Container',
		to: 'body',
		after_open: () => {},
		before_close: () => {},
		after_close: () => {},
	});

	// Sliders
	$('#reviews').multiSlider({
		slider: '.slider',
		slide_class: '.slide-item',
		nav: '.nav',
		item: 2,
		item_sliding: 2,
		loop: true,
		dots: true,
		automove: false,
		interval: 2000,
		transition: '0.5s',
		response: {
			'0': {
				item: 1,
				item_sliding: 1,
				dots: false,
				loop: true,
			},
			'480': {
				item: 1,
				item_sliding: 1,
				dots: false,
			},
			'768': {
				item: 2,
				item_sliding: 2,
				loop: false,
			},
			'992': {
				item: 3,
				item_sliding: 1,
				dots: true,
				loop: true,
			},
		},
	});
	$('#for-client').multiSlider({
		slider: '.slider',
		slide_class: '.slide-item',
		nav: '.nav',
		item: 1,
		item_sliding: 1,
		loop: true,
		dots: true,
		automove: false,
		interval: 2000,
		transition: '0.5s',
		response: true,
	});

	$('#banner-slider').multiSlider({
		slider: '.slider',
		slide_class: '.slide-item',
		nav: '.nav',
		item: 1,
		item_sliding: 1,
		loop: true,
		dots: false,
		automove: true,
		interval: 5000,
		transition: '0.5s',
		tabs: false,
		futurama: false,
	});

	// Product count estimate
	function count(count_control) {
		let input = $('.form-control');
		input.keyup(function() {
			val = $(this).val();
		});
		let count_control_el = $(count_control);
		count_control_el.click(function() {
			let ths = $(this);
			let input = ths.parent().find('.form-control');
			let val = +input.val();
			switch (ths.data('operator')) {
				case '+':
					input.val((val += 1));
					break;
				case '-':
					if (val > 1) {
						input.val((val -= 1));
					}
					break;
			}
			input.val(val);
		});
	}
	count('.item-count-control');

	// Open cart product with ajax
	// $('.get-product').click(function() {
	// 	var data = $(this).data();
	// 	$.post('/ajaxresources', data, res => {
	// 		$('#product-modal').append(res);
	// 	}).done(res => {
	// 		$('#product-slider').slider({
	// 			slide_class: '.slide-item',
	// 			nav: '.nav',
	// 			item: 1,
	// 			item_sliding: 1,
	// 			loop: true,
	// 			dots: true,
	// 			automove: false,
	// 			interval: 2000,
	// 			transition: '0.5s',
	// 			response: true,
	// 		});
	// 		let product_modal = $('#product-modal');
	// 		product_modal.addClass('product-load');
	// 		count('.cart-count-control');
	// 	});
	// });

	// Notification online / offline
	let offline = $('.offline'),
		online = $('.online');
	window.addEventListener(
		'online',
		function(e) {
			$('.notification').removeClass('notification-visible');
			online.addClass('notification-visible');
			setTimeout(function() {
				online.removeClass('notification-visible');
			}, 5000);
		},
		false
	);
	window.addEventListener(
		'offline',
		function(e) {
			$('.notification').removeClass('notification-visible');
			offline.addClass('notification-visible');
			setTimeout(function() {
				offline.removeClass('notification-visible');
			}, 5000);
		},
		false
	);

	// Maskedinput
	$('.mask-phone').mask('+7 (999) 999-99-99');
	$('#phone').mask('+7 (999) 999-99-99');

	// Preloader
	let preload = $('.inner-preload'),
		width = 0,
		timeout = 2000,
		inter = 50,
		interval = setInterval(function() {
			width += (inter / timeout) * 100;
			preload.css('width', `${width}%`);
		}, inter);
	setTimeout(function() {
		let preloader = $('.preloader');
		preloader.fadeOut();
		clearInterval(interval);
		preload.css('width', '100%');
	}, timeout);
});

$(window).on('load', function() {
	let body = $('body');
	body.addClass('ready');
});
